package vs.model;

public class Csapat {
	
	private String nev;

	public Csapat(String nev) {
		super();
		this.nev = nev;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Csapat : ");
		builder.append(nev);
		return builder.toString();
	}
	
	
	
	

}
