package vs.controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/vs/view/Scene.fxml"));

		Scene scene = new Scene(root);
		//scene.getStylesheets().add("/styles/Styles.css");

		stage.setTitle("Foci EB 2");
		stage.setScene(scene);
		stage.show();
	}

		
	public static void main(String[] args) {
		launch(args);
	}

}
