package vs.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import vs.dao.FileDao;
import vs.model.Csapat;
import vs.model.Par;

public class FXMLController implements Initializable {

	ArrayList<Csapat> csapatok = new ArrayList<>();
	ArrayList<Par> parok = new ArrayList<>();
	int osszesNezettMeccs = 0; // ez a meccsenkénti sör átlaghoz kell

	@FXML
	private Label lbSor;

	@FXML
	private Label lbPerc;

	@FXML
	private Label lbCsapatok;

	@FXML
	private TextArea taHazasparok;

	@FXML
	private TextArea taFerjek;

	@FXML
	private TextArea taFelesegek;

	@FXML
	private Button btnMeccs;

	public void pressedMeccsBtn(ActionEvent event) {
	//	final int MECCSHOSSZ = 90;
		Random rnd = new Random();
		String csapat1 = "";
		String csapat2 = "";
		csapat1 = csapatok.get(rnd.nextInt(csapatok.size())).getNev();
		while ((csapat2 = csapatok.get(rnd.nextInt(csapatok.size())).getNev()).equals(csapat1)) {
		}
		lbCsapatok.setText(csapat1 + " , " + csapat2 + " :-)");
		//int teljesMeccsHossz = MECCSHOSSZ + rnd.nextInt(20);
		boolean joMeccs = rnd.nextBoolean();
		if (joMeccs) {
			osszesNezettMeccs++;
		}
		if (rnd.nextBoolean()) {
			int melyikPar = rnd.nextInt(parok.size());
			parok.get(melyikPar).meccs(rnd.nextInt(90) + 10, joMeccs);
			refreshTexts();
			int sorAtlag = 0;
			int osszSzabadIdo = 0;
			for (Par par : parok) {
				if (par.getSorokSzama() > 0) {
					sorAtlag += par.getSorokSzama();
				}
				osszSzabadIdo += par.getSzabadIdo();
			}
			if (osszesNezettMeccs>0) {
			sorAtlag = sorAtlag / osszesNezettMeccs;
			}; 
			lbPerc.setText("Összesen " + osszSzabadIdo + " perc.");
			lbSor.setText("Átlagosan " + sorAtlag + " sor fogyott.");

		} else {
			lbCsapatok.setText(csapat1 + " , " + csapat2 + "- Nem nézik..");
			lbCsapatok.setAlignment(Pos.CENTER);

		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FileDao fd = new FileDao();
		try {
			csapatok = fd.readCsapat("d:\\csapatok.txt");
			parok = fd.readPar("d:\\parok.txt");
			refreshTexts();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void refreshTexts() {
		String kiirniFerfi = "";
		String kiirniParok = "";
		String kiirniNo = "";
		for (Par par : parok) {
			kiirniFerfi += par.getFerfiNev() + " " + par.getSorokSzama() + " sört ivott.\n\r";
			kiirniNo += par.getNoiNev() + " szabadideje: " + par.getSzabadIdo() + " perc.\n\r";
			kiirniParok += par.getFerfiNev() + " - " + par.getNoiNev() + "\n\r";
		}
		taFerjek.clear();
		taFelesegek.clear();
		taHazasparok.clear();
		taFerjek.appendText(kiirniFerfi);
		taFelesegek.appendText(kiirniNo);
		taHazasparok.appendText(kiirniParok);
	}
}
