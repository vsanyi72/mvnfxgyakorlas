package vs.dao;

import java.io.IOException;
import java.util.ArrayList;

import vs.model.Csapat;
import vs.model.Par;


public interface Dao {
	
				
		public ArrayList<Csapat> readCsapat(String path) throws IOException;
		public ArrayList<Par> readPar(String path) throws IOException;
}
